import { By, ThenableWebDriver } from 'selenium-webdriver';
import { INewablePage, Page } from './pom';

export const delay = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

export function css(selector: string) {
  return (target: Page, propertyKey: string) => {
    Object.defineProperty(target, propertyKey, {
      configurable: true,
      enumerable: true,
      get() {
        return this.driver.findElement(By.css(selector));
      },
    });
  };
}

export function forPage<T extends Page>(page: INewablePage<T>): (driver: ThenableWebDriver) => Promise<boolean> {
  return (driver: ThenableWebDriver) => {
    const condition = new page(driver).loadCondition();
    return condition(driver);
  };
}
