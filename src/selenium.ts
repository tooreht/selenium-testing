// Common Imports
import Debug = require('debug');
import { parse } from 'path';
import { Browser, Builder } from 'selenium-webdriver';
import chrome = require('selenium-webdriver/chrome');
import edge = require('selenium-webdriver/edge');
import firefox = require('selenium-webdriver/firefox');
import ie = require('selenium-webdriver/ie');
// import remote = require('selenium-webdriver/remote');
import safari = require('selenium-webdriver/safari');

import { SeleniumBrowser } from './browser';
import { Browser as WebBrowser, IBrowserstackOptions, ILocalOptions, IOptions } from './options';

const pkg = require('../package.json');
const debug = Debug(`${pkg.name}:${parse(__filename).name}`);
const DEFAULT_OPTIONS: IOptions = {
  browserstack: {
    capabilities: [],
  },
  local: {
    browsers: getAvailableBrowsers(),
  },
};

// TODO: Cleanup this file, maybe in to dedicated files for local and browserstack

// Browserstack Imports

export function setup(options?: any): SeleniumBrowser[] {
  const o: IOptions = {
    ...DEFAULT_OPTIONS,
    ...options,
  };

  switch (o.mode) {
    case 'browserstack': {
      return browserstack(o.browserstack);
    }
    case 'local': {
      return local(o.local);
    }
    case 'detect': {
      if (process.env.BROWSERSTACK_USERNAME && process.env.BROWSERSTACK_ACCESS_KEY) {
        return browserstack(o.browserstack);
      } else {
        return local(o.local);
      }
    }
    case 'all':
    default: {
      return local(o.local).concat(browserstack(o.browserstack));
    }
  }
}

export function configureBuilder(capabilities: any) {
  const builder = new Builder()
    // .forBrowser(capabilities.browserName)
    .withCapabilities(capabilities);
  // .setChromeOptions(new chrome.Options().headless().windowSize(screen)) // TODO: only for local env
  // .setFirefoxOptions(new firefox.Options().headless().windowSize(screen)) // TODO: only for local env

  if (process.env.BROWSERSTACK_USERNAME && process.env.BROWSERSTACK_ACCESS_KEY) {
    builder.usingServer('http://hub-cloud.browserstack.com/wd/hub');
  }

  return builder;
}

// Browserstack Vars

// Local

function local(options: ILocalOptions) {
  const browsers = [];
  for (const browser of options.browsers) {
    browsers.push(new SeleniumBrowser(browser, configureBuilder({ browserName: browser })));
  }
  return browsers;
}

function getAvailableBrowsers() {
  debug(`Searching for WebDriver executables installed on the current system...`);

  const targets = [
    // @ts-ignore
    [chrome.locateSynchronously, Browser.CHROME],
    // @ts-ignore
    [edge.locateSynchronously, Browser.EDGE],
    // @ts-ignore
    [firefox.locateSynchronously, Browser.FIREFOX],
    // @ts-ignore
    [ie.locateSynchronously, Browser.IE],
    // @ts-ignore
    [safari.locateSynchronously, Browser.SAFARI],
  ];

  const availableBrowsers: WebBrowser[] = [];
  for (const pair of targets) {
    const [fn, name] = pair;
    if (fn()) {
      debug(`... located ${name}`);
      availableBrowsers.push(name);
    }
  }

  if (availableBrowsers.length === 0) {
    debug(`Unable to locate any WebDriver executables for testing`);
  }

  return availableBrowsers;
}

// Browserstack

export function browserstack(options: IBrowserstackOptions) {
  const browsers = [];
  for (const capability of options.capabilities) {
    browsers.push(new SeleniumBrowser(capability.browserName, configureBuilder(capability)));
  }
  return browsers;
}
