export type Browser = 'chrome' | 'edge' | 'firefox' | 'ie' | 'safari';

export type Mode = 'browserstack' | 'local' | 'all' | 'detect';

export interface ILocalOptions {
  browsers: Browser[];
}

export interface IBrowserstackCapability {
  [key: string]: string;
}

export interface IBrowserstackOptions {
  capabilities: IBrowserstackCapability[];
}

/**
 * The setup options interface:
 *
 * let options:IOptions = {
 *     browserstack: {
 *         capabilities: [
 *             {
 *                 'build': require('./package.json').version,
 *                 'project': require('./package.json').name,
 *                 'os': 'OS X',
 *                 'os_version': 'High Sierra',
 *                 'browserName': 'Chrome',
 *                 'browser_version': '66.0',
 *                 'browserstack.local': 'false',
 *                 'browserstack.debug': 'true',
 *                 'browserstack.networkLogs': 'true',
 *                 'browserstack.timezone': 'CH',
 *                 'browserstack.selenium_version': '3.10.0',
 *                 'browserstack.user': process.env.BROWSERSTACK_USERNAME || '',
 *                 'browserstack.key': process.env.BROWSERSTACK_ACCESS_KEY || '',
 *             },
 *             {
 *                 'build': require('./package.json').version,
 *                 'project': require('./package.json').name,
 *                 'os': 'Windows',
 *                 'os_version': '10',
 *                 'browserName': 'Firefox',
 *                 'browser_version': '64.0',
 *                 'browserstack.local': 'false',
 *                 'browserstack.debug': 'true',
 *                 'browserstack.networkLogs': 'true',
 *                 'browserstack.timezone': 'CH',
 *                 'browserstack.selenium_version': '3.10.0',
 *                 'browserstack.user': process.env.BROWSERSTACK_USERNAME || '',
 *                 'browserstack.key': process.env.BROWSERSTACK_ACCESS_KEY || '',
 *             },
 *         ]
 *     },
 *     local: {
 *         browsers: ['firefox']
 *     }
 * };
 */
export interface IOptions {
  browserstack: IBrowserstackOptions;
  local: ILocalOptions;
  mode?: Mode;
}
