import { ThenableWebDriver } from 'selenium-webdriver';

export interface INewablePage<T extends Page> {
  new (driver: ThenableWebDriver): T;
}

export abstract class Page {
  protected driver: ThenableWebDriver;
  private url: string = '';

  public constructor(driver: ThenableWebDriver) {
    this.driver = driver;
  }

  public async navigate(): Promise<void> {
    await this.driver.navigate().to(this.url);
    await this.driver.wait(this.loadCondition());
  }

  public abstract loadCondition(): (driver: ThenableWebDriver) => Promise<boolean>;

  protected setUrl(url: string) {
    this.url = url;
  }
}
