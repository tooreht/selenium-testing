import { setup } from '../selenium';
import { WebDriver } from 'selenium-webdriver';

describe('Local browsers', () => {
  test('Browsers start using getter.', async () => {
    expect.assertions(1);

    const browsers = setup();

    for (let browser of browsers) {
      await expect(browser.driver).resolves.toBeInstanceOf(WebDriver);
      browser.driver = null; // stop driver
    }
  });

  test('Browsers start and stop correctly.', async () => {
    expect.assertions(2);

    const browsers = setup();

    for (let browser of browsers) {
      await expect(browser.start()).resolves.toBeInstanceOf(WebDriver);
      // expect(await browser.name).toBe('chrome'); // TODO: Add when browsers are configurable
      await expect(browser.stop()).resolves.toBeUndefined();
    }
  });

  test('Browsers restart correctly.', async () => {
    expect.assertions(4);

    const browsers = setup();

    for (let browser of browsers) {
      const b1 = await browser.start();
      expect(b1).toBeInstanceOf(WebDriver);

      const b2 = await browser.restart();
      expect(b2).toBeInstanceOf(WebDriver);

      const session1 = await b1.getSession();
      const session2 = await b2.getSession();
      expect(session1).not.toEqual(session2);

      await expect(browser.stop()).resolves.toBeUndefined();
    }
  });
});
