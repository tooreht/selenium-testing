import { setup } from '../selenium';

describe('Primary exports', () => {
  test('setup() return value.', () => {
    expect.assertions(1);
    expect(setup()).toHaveLength(1);
  });
});
