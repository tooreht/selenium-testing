import { Builder, ThenableWebDriver } from 'selenium-webdriver';

export class SeleniumBrowser {
  public name: string;
  private builder: Builder;
  private _driver: ThenableWebDriver | null;

  constructor(name: string, builder: Builder) {
    this.name = name.toLowerCase();
    this.builder = builder;
    this._driver = null;
  }

  get driver() {
    if (this._driver === null) {
      this._driver = this.builder.build();
    }
    return this._driver;
  }

  set driver(driver: ThenableWebDriver | null) {
    if (this._driver !== null) {
      this._driver.quit();
    }
    this._driver = driver;
  }

  public start() {
    return this.driver!;
  }

  public stop() {
    try {
      return this.driver!.quit();
    } catch (e) {
      return Promise.reject(e);
    } finally {
      this._driver = null;
    }
  }

  public async restart() {
    try {
      await this.stop();
      return this.start();
    } catch (e) {
      return Promise.reject(e);
    }
  }
}
