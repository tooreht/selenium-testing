# Selenium Testing

Small library with the aim to provide unobtrusive functionality for setting up and running selenium tests with the official [selenium-webdriver](https://github.com/SeleniumHQ/selenium/tree/master/javascript/node/selenium-webdriver) library.